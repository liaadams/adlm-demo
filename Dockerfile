FROM alpine
RUN apk update
RUN apk add -y openjdk8
EXPOSE 8090
ADD target/assessment-survey-demo-0.0.1-SNAPSHOT.jar assessment.jar
ENTRYPOINT ["java","-jar","assessment.jar"]