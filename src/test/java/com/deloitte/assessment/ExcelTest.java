package com.deloitte.assessment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import com.deloitte.assessment.model.Question;
import com.deloitte.assessment.model.Response;

public class ExcelTest {

////	@Test
//	public void loadRolesTest() throws IOException {
//		try (InputStream excelFile = getClass().getResourceAsStream("/Roles.xlsx")) {
//			Workbook workbook = new XSSFWorkbook(excelFile);
//			Sheet sheet = workbook.iterator().next();
//			Iterator<Row> rowIterator = sheet.iterator();
//			rowIterator.next();
//			while (rowIterator.hasNext()) {
//				Row currentRow = rowIterator.next();
//				String roleName = currentRow.getCell(0).getStringCellValue();
////				Cell cell = currentRow.getCell(1);
////				cell = currentRow.getCell(2);
////				cell = currentRow.getCell(5);
//				boolean agileProgram = 1 == currentRow.getCell(1).getNumericCellValue();
//				boolean agileDeliv = 1 == currentRow.getCell(2).getNumericCellValue();
//				boolean agilePort = 1 == currentRow.getCell(5).getNumericCellValue();
//			}
//		}
//	}
//	
//	@Test
//	public void createExcelTest() {
//		List<Response> responses = new ArrayList<>();
//		Response response = new Response();
//		Question question = new Question();
//		question.setText("Q1");
//		response.setQuestion(question);
//		response.setContent("A1");
//		responses.add(response);
//		response = new Response();
//		question = new Question();
//		question.setText("Q2");
//		response.setQuestion(question);
//		response.setContent("A2");
//		responses.add(response);
//		XSSFWorkbook workbook = new XSSFWorkbook();
//        XSSFSheet sheet = workbook.createSheet("responses");
//        int rowNum = 0, colNum = 0;
//        Row row = sheet.createRow(rowNum++);
//        Cell cell = row.createCell(colNum++);
//        cell.setCellValue("Question");
//        cell = row.createCell(colNum++);
//        cell.setCellValue("Response");
//        for(Response r : responses) {
//        	colNum = 0;
//        	row = sheet.createRow(rowNum++);
//        	cell = row.createCell(colNum++);
//            cell.setCellValue(r.getQuestion().getText());
//            cell = row.createCell(colNum++);
//            cell.setCellValue(r.getContent());
//        }
//        String tmpdir = System.getProperty("java.io.tmpdir");
//        System.out.println(tmpdir);
//        try {
//        	FileOutputStream outputStream = new FileOutputStream(tmpdir + "response.xlsx");
//            workbook.write(outputStream);
//            workbook.close();
//            outputStream.close();
//        } catch (Exception e) {
//        	e.printStackTrace();
//        }
//        File f = new File(tmpdir + "response.xlsx");
//	}
}
