package com.deloitte.assessment;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.deloitte.assessment.model.Employee;
import com.deloitte.assessment.model.Role;
import com.deloitte.assessment.repository.EmployeeRepository;
import com.deloitte.assessment.repository.SurveyRepository;
import com.deloitte.assessment.service.SurveyService;

public class SurveyServiceTest {
	
	@Mock
	private SurveyRepository surveyRepository;
	
	@Mock
	private EmployeeRepository empRepository;
	
	@InjectMocks
	private SurveyService surveyService;
	
	private Employee emp;
	
	@Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        emp = new Employee();
        emp.setRoles(new ArrayList<Role>());
    }

	@Test
	public void getSurveysTest() {
		surveyService.getSurvey("djsbsjkn");
		surveyService.getSurveys(emp);
	}
	
	@Test
	public void addSurveysTest() {
		surveyService.addSurveys(emp);
		surveyService.addSurveys(emp, new ArrayList<String>());
	}
}
