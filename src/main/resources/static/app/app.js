var app = angular.module('assessmentApp', [ 'restangular', 'ui.bootstrap', 'ui.router', 'ngMessages']);

app.config( function(RestangularProvider) {
	RestangularProvider.setBaseUrl('http://localhost:8090/assessment/');
//	RestangularProvider.setBaseUrl('http://azealadvwb01.dir.dcloudservice.com/');
});

app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
//	$locationProvider.html5Mode(true);
	$locationProvider.hashPrefix('');
	  $stateProvider
	    .state('homeState', {
	      url: "/home",
	      templateUrl: 'app/views/home.html',
	      controller: 'homeCtrl'
	    })
	    
	    .state('adminState', {
	      url: "/admin",
	      templateUrl: "app/views/admin.html",
	      controller: 'adminCtrl'
	    })
	  
	    .state('menuState', {
	      url: "/menu",
	      templateUrl: "app/views/menu.html",
	      controller: 'menuCtrl'
	    })
	  
	    .state('surveyMenuState', {
	      url: "/survey-menu/:empId",
	      templateUrl: "app/views/survey-menu.html",
	      controller: 'survMenuCtrl',
	      hideNavbar: true
	    })
	    
//	    .state('surveyState', {
//	      url: "/survey/:survId",
//	      templateUrl: "app/views/survey.html",
//	      controller: 'survCtrl',
//	      hideNavbar: true
//	    })
	    $urlRouterProvider.otherwise("/home");
});

//app.config(function($routeProvider, $locationProvider) {
//$locationProvider.hashPrefix('');
//$routeProvider.when('/', {
//	templateUrl : 'app/views/home.html',
//	controller : 'homeCtrl'
//})
//
//.when('/admin', {
//	templateUrl : 'app/views/admin.html',
//	controller : 'adminCtrl'
//})
//
//.when('/menu', {
//	templateUrl : 'app/views/menu.html',
//	controller : 'menuCtrl'
//})
//
//});