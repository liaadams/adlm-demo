app.factory('appService', [ 'Restangular', function(Restangular) {
	var service = {
		getSegments : getSegments,
		getTeams : getTeams,
		getRoles : getRoles,
		getOrganizations : getOrganizations,
		postForm : postForm,
		getSurveys : getSurveys,
		getSurvey : getSurvey,
		getEmployee: getEmployee,
		getQuestions : getQuestions
	};

	function getSegments() {
		return Restangular.all('segments').getList();
	}

	function getTeams() {
		var resp = Restangular.all('api/teams').customGET();
		return resp;
	}
	
	function getRoles() {
		return Restangular.all('api/roles').customGET();
	}
	
	function getOrganizations() {
		return Restangular.all('api/organizations').customGET();
	}

	function postForm(data) {
		return Restangular.all('').post(data);
	}
	
	function getSurveys(empId) {
		return Restangular.all('api/surveys/emp/' + empId).customGET();
	}
	
	function getSurvey(survId) {
		return Restangular.all('api/surveys/' + survId).customGET();
	}
	
	function getEmployee(empId) {
		return Restangular.all('api/employees/' + empId).customGET();
	}
	
	function getQuestions(params) {
		return Restangular.all('api/questions').customGET("", params);
	}

	return service;
} ]);