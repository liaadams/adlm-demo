app.controller('menuCtrl', [ '$scope', '$window', 'appService', '$http',
		function($scope, $window, appService, $http) {

			$scope.selectedTeams = [];
			$scope.selectedTeams.push('Select Team')
			$scope.teamDisplay = [];
			$scope.selectedTeam = null;
			$scope.roleDisplay = [];
			$scope.roles = [];
			$scope.selectedRoles = [];
			$scope.selectedRole = null;
			$scope.selectedRoles.push('Select Role');
			$scope.organization = null;
			$scope.organizations = [];
			$scope.exportType = null;
			$scope.exportOption = null;
			$scope.segment = null;
			
			$scope.exportTypes = ['Role', 'Level'];
			$scope.exportOptions = [];
			
			loadTeams();
			loadRoles();
			loadOrganizations();
			
			function loadRoles() {
				appService.getRoles().then(function(response) {
					response.forEach(function(role) {
						if(!(role.name === 'Portfolio Admin' || role.name === 'Program Admin' 
							|| role.name === 'Team Admin' || role.name === 'Continuous Delivery Admin')) {
							$scope.roles.push(role);
							$scope.roleDisplay.push(role.name);
						}
					});
				});
			}

			function loadTeams() {
				appService.getTeams().then(function(response) {
					$scope.enumMap = response.plain();
					for ( var key in $scope.enumMap) {
						if(key == 'PortfolioManagement' || key == 'ProgramManagement' || key == 'Delivery' || key == 'ContinuousDelivery')
							$scope.teamDisplay.push($scope.enumMap[key]);
					}
				});
			}
			
			var segments = {};
			function loadOrganizations() {
				appService.getOrganizations().then(function(response) {
					$scope.organizations = response;
					response.forEach(function(org){
//						$scope.organizations.push(org.name);
//						segments[org.name] = org.segments;
					});
				});
			}
			
//			$scope.getSegments = function() {
//				$scope.segments = segments[$scope.organization.name];
//			}
			
			$scope.getExportOptions = function() {
				if($scope.exportType == 'Role') {
					$scope.exportOptions = $scope.roleDisplay;
				}
				else {
					$scope.exportOptions = $scope.teamDisplay;
				}
			}
			
			$scope.exportResponses = function() {
//				$window.open('http://localhost:8090/assessment/api/responses/export/' + $scope.organization.name);
//						$window.open('http://azealadvwb01.dir.dcloudservice.com/api/responses/export/' + $scope.organization);
				$http({
					method: 'GET',
					url: 'http://localhost:8090/assessment/api/responses/export/' + $scope.organization.name,
//					url: 'http://azealadvwb01.dir.dcloudservice.com/api/responses/export/' + $scope.organization.name,
					responseType: 'blob'
				}).then(function(success){
//					application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
					var blob = new Blob([success.data], {type: 'application/vnd.openxml'});
//					saveAs(blob, $scope.organization.name + '_export.xlsx')
					var downloadUrl = URL.createObjectURL(blob);
			        var a = document.createElement("a");
			        a.href = downloadUrl;
			        a.download = $scope.organization.name + '_export.xlsx';
			        document.body.appendChild(a);
			        a.click();
				}, function(error) {
					console.log('error');
				});
			}
			
			var baseUrl = 'http://localhost:8090/assessment/survey/';
//			var baseUrl = 'http://azealadvwb01.dir.dcloudservice.com/survey/';
			$scope.getTeamQuestions = function() {
				$window.location.href = baseUrl + 'team' + '/' + $scope.selectedTeam;
			}
			
			$scope.getRoleQuestions = function() {
				$window.location.href = baseUrl + 'role' + '/' + $scope.selectedRole;
			}
			
		} ]);