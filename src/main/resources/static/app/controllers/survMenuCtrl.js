app.controller('survMenuCtrl', [
		'$scope',
		'$location',
		'$window',
		'$stateParams',
		'appService',
		function($scope, $location, $window, $stateParams, appService) {
			$scope.selectedSurvey;
			$scope.surveys = [];
//			var empId = $location.path().split("/")[3];
			appService.getSurveys($stateParams.empId).then(function(response) {
				response.forEach(function(s) {
					if(!s.isCompleted)
						$scope.surveys.push(s);
				});
				$scope.survNames = $scope.surveys.map(function(a) {return a.roleName});
			});
			
			appService.getEmployee($stateParams.empId).then(function(response){
				$scope.employee = response[0];	
			});
			$scope.getSurvey = function() {
				$window.location.href = $location.protocol() + '://localhost:' + $location.port()
						+ '/assessment/survey/' +  $scope.selectedSurvey.survId;
//				$window.location.href = $location.protocol() + '://'+ $location.host()
//				+ '/survey/' +  $scope.selectedSurvey.survId;
			}
		}]);