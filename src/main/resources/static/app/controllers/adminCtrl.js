app.controller('adminCtrl', [
		'$scope',
		'appService',
		function($scope, appService) {

			$scope.roleDisplay = [];
			
			function initForm() {
				var segments = [];
				var employees = [];
				var roles = [];
				roles.push({});
				
				employees.push({
					roles: roles
				});
				segments.push({
					employees: employees
				});
				$scope.organization = {
						segments: segments
				};
				$scope.selectedRoles = [];
//				$scope.selectedRoles.push('Select Role');
			}
			
			initForm();
			loadRoles();
			var roleMap = {};
			
//			function loadTeams() {
//				appService.getTeams().then(function(response) {
//					$scope.enumMap = response.plain();
//					for ( var key in $scope.enumMap) {
//						if(key == 'PortfolioManagement' || key == 'ProgramManagement' || key == 'Delivery' || key == 'ContinuousDelivery')
//							$scope.teamDisplay.push($scope.enumMap[key]);
//					}
//				});
//			}
			
			function loadRoles() {
				appService.getRoles().then(function(response) {
					response.forEach(function(role) {
						roleMap[role.name] = role;
						$scope.roleDisplay.push(role.name);
					});
				});
			}

			$scope.addMember = function(index) {
				$scope.organization.segments[0].employees.push({
					roles : []
				});
			};

			$scope.submit = function() {
				$scope.showSuccess = false;
				$scope.showError = false;
				$scope.organization.segments[0].employees.forEach(function(employee, i) {
					var role = roleMap[$scope.selectedRoles[i]];
					if(i == 0)
						employee.roles[0] = role;
					else
						employee.roles.push(role);
				});
				
//				var errorTag = document.getElementById("formError");
//				document.getElementById("formSuccess");​
				 appService.postForm($scope.organization).then(function success(response) {
					  document.getElementById("formSuccess").innerHTML = 'Emails sent to team members';
//					  document.getElementById("formError");
	                  $scope.errorMessage = '';
//	                  $scope.submitted = false;
	                  $scope.showSuccess = true;
					}, function error(response) {
						var errorTag = document.getElementById("formError");
						if (response.status == 409) {
		                    $scope.errorMessage = response.data[0];
		                  }
		                  else {
		                    $scope.errorMessage = 'Error creating org';
		                  }
					      errorTag.innerHTML = $scope.errorMessage;
		                  $scope.showError = true;
					});
				 $scope.organization = {};
				 $scope.orgForm.$setPristine();
				 $scope.orgForm.$setUntouched();
				 initForm();
			};

		} ]);