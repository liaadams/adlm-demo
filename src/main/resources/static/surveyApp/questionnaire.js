$(document)
		.ready(
				function() {
					Survey.defaultBootstrapCss.navigationButton = "btn btn-primary";
					Survey.Survey.cssType = "bootstrap";

					var title;
					pages = [];

					if (surveyType === "questionnaire") {
						title = questions[0].teamType.name;
						for (var i = 0; i < questions.length; i++) {
							var name = questions[i].id.toString();
							var page = {
								title : questions[i].text,
								questions : [ {
									type : "comment",
									name : name,
									title : "Response: "
								} ]
							};
							pages.push(page);
						}
					}
					else if (surveyType === "maturity") {
						//title = 'tbd';
						for (var i = 0; i < questions.length; i++) {
							var name = questions[i].id.toString();
							var page = {
								title : questions[i].text,
								questions : [ {
									type : "radiogroup",
									name : name,
									title : "Response: ",
									choices: ["Yes", "No"]
								} ]
							};
							pages.push(page);
						}
					}

					var survey = new Survey.Model({
						//title : title,
						//showProgressBar: "bottom", 
						goNextPageAutomatic: true,
						pages : pages
					});

					function doOnCurrentPageChanged(survey) {
						document.getElementById('surveyProgress').innerText = "Page "
								+ (survey.currentPage.visibleIndex + 1)
								+ " of " + survey.visiblePageCount + ".";
						if (document.getElementById('surveyPageNo'))
							document.getElementById('surveyPageNo').value = survey.currentPageNo;
					}

					function getQuestionById(id) {
						var ret;
						questions.forEach(function(q) {
							if (q.id == id)
								ret = q;
						});
						return ret;
					}
					responses = [];
					var surveyValueChanged = function(sender, options) {
						var val = options.question.value;
						var question = getQuestionById(options.name);
						var response = {
							content : val,
							question : question,
							employee : null
						};
						responses.push(response);
					};

					var sendResponses = function(sender) {
						
						var url;
						if (surveyType === "questionnaire") {
							url = "http://localhost:8090/assessment/survey/"+ uuid;
//							url = "http://azealadvwb01.dir.dcloudservice.com/survey/"+ uuid;
							responses
							.forEach(function(response) {
								response.question.teamType = response.question.teamType.$name;
							});
						}
						else if (surveyType === "maturity") {
							url = "http://localhost:8090/assessment/survey/"+ uuid;
//							url = "http://azealadvwb01.dir.dcloudservice.com/survey/"+ uuid;
						}
						$.ajax({
							contentType : 'application/json',
							type : "POST",
							url : url,
							dataType : "json",
							async : false,
							data : JSON.stringify(responses),
							success : function() {
								alert("Data successfully submitted");
							}
						});
					};

					$("#surveyElement").Survey({
						model : survey,
						onValueChanged : surveyValueChanged,
						onComplete : sendResponses,
						onCurrentPageChanged : doOnCurrentPageChanged
					});
					doOnCurrentPageChanged(survey);
				});