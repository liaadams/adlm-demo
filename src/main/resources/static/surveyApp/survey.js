$(document)
		.ready(
				function() {
					Survey.defaultBootstrapCss.navigationButton = "btn btn-primary";
					Survey.Survey.cssType = "bootstrap";

					var title;
					var questions = [];
					pages = [];
					var employee;
					var quesUrl = 'http://localhost:8090/assessment/api/questions';
//					 var quesUrl = 'http://azealadvwb01.dir.dcloudservice.com/api/questions';

					var reqBody;
					var successFunction;
//					if (surveyType == 'role') {
//						reqBody = {
//							'employee' : uuid
//						};
//						successFunction = function(data) {
//							questions = data;
//							getEmployee();
//						};
//					} else if (surveyType == 'team') {
//						reqBody = {
//							'TeamType' : team	
//						};
//						successFunction = function(data) {
//							questions = data;
//							getEmployee();
//						}
//					} else if (team !== null) {
//						reqBody = {
//							'TeamType' : team
//						};
//						successFunction = function(data) {
//							questions = data;
//							createSurvey();
//						};
//					} else if (role !== null) {
						reqBody = {
							'role' : role
						};
						successFunction = function(data) {
							questions = data;
							createSurvey();
						};
//					}

					$.get({
						url : quesUrl,
						data : reqBody,
						success : successFunction
					});

					function getEmployee() {
						var empUrl = 'http://localhost:8090/assessment/api/employees/'+ uuid;
//						 var empUrl = 'http://azealadvwb01.dir.dcloudservice.com/api/employees/'+ uuid;
						$.get({
							url : empUrl,
							success : function(data) {
								employee = data;
								createSurvey();
							}
						});
					}

					function createSurvey() {
						// console.log(JSON.stringify(questions));

						// if (surveyType === "maturity") {
						// title = 'tbd';
						var capPageMap = {};
						for (var i = 0; i < questions.length; i++) {
							var name = questions[i].id.toString();
							var page = capPageMap[questions[i].capability.capabilityType];
							if (page === undefined) {
								var page = {
									title : questions[i].capability.capabilityType,
									questions : [ {
										type : "radiogroup",
										name : name,
										title : questions[i].text,
										choices : [ "Yes", "No" ]
									} ]
								};
								capPageMap[questions[i].capability.capabilityType] = page;
								pages.push(page);
							} else {
								page.questions.push({
									type : "radiogroup",
									name : name,
									title : questions[i].text,
									choices : [ "Yes", "No" ]
								});
							}
						}
						// }

						pages.sort(function(a, b) {
							a.title.localeCompare(b.title);
						});

						var survey = new Survey.Model({
							// title : title,
							 showProgressBar: "top",
							goNextPageAutomatic : true,
							pages : pages,
							sendResultOnPageNext : true
						});

						responses = [];
						questions.forEach(function(question) {
							if (question.id == 73) {
								console.log('found question');
							}
							var response = question.response[0];
							if (response) {
								console.log('saved response');
								survey.setValue(question.id.toString(),
										response.contents);
							}
						});

						function doOnCurrentPageChanged(survey) {
							document.getElementById('surveyProgress').innerText = "Page "
									+ (survey.currentPage.visibleIndex + 1)
									+ " of " + survey.visiblePageCount + ".";
							if (document.getElementById('surveyPageNo'))
								document.getElementById('surveyPageNo').value = survey.currentPageNo;
						}

						function saveResponses() {
							var url = "http://localhost:8090/assessment/api/responses/"+ uuid;
//							var url = "http://azealadvwb01.dir.dcloudservice.com/survey/"+ uuid;
							console.log(JSON.stringify(responses));
							$.ajax({
								contentType : 'application/json',
								type : "POST",
								url : url,
								dataType : "json",
								async : false,
								data : JSON.stringify(responses),
								success : function() {
									alert("Data successfully submitted");
									responses = {};
								}
							});

						}

						function getQuestionById(id) {
							var ret;
							questions.forEach(function(q) {
								if (q.id == id)
									ret = q;
							});
							return ret;
						}

						reponses = [];
						var surveyValueChanged = function(sender, options) {
							var val = options.question.value;
							var question = getQuestionById(options.name);
							var response = {
								content : val,
								question : question,
								employee : employee
							};
//							responses = [];
							responses.push(response);
//							saveResponses();
						};

						var sendResponses = function(sender) {
							 var url = "http://localhost:8090/assessment/survey/"+ uuid;
//							 var url = "http://azealadvwb01.dir.dcloudservice.com/survey/"+ uuid;

							console.log(JSON.stringify(responses));
							$.ajax({
								contentType : 'application/json',
								type : "POST",
								url : url,
								dataType : "json",
								async : false,
								data : JSON.stringify(responses),
								success : function() {
									alert("Data successfully submitted");
								}
							});
						};

						function surveySendResult(survey) {
							console.log(JSON.stringify(survey.data));
						}
						
						var myCss = {
								matrix: {root: "table table-striped"}
						};

//						if (surveyType == 'role' || surveyType == 'team')
							$("#surveyElement").Survey({
								model : survey,
								onValueChanged : surveyValueChanged,
								onComplete : sendResponses,
//								onCurrentPageChanged : doOnCurrentPageChanged,
								css: myCss
								//onSendResult : surveySendResult
							});
//						else
//							$("#surveyElement").Survey({
//								model : survey,
//								onCurrentPageChanged : doOnCurrentPageChanged
//							});
						// doOnCurrentPageChanged(survey);
					}
				});