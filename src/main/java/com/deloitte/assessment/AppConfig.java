package com.deloitte.assessment;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.deloitte.assessment.model.Capability;
import com.deloitte.assessment.model.CapabilityType;
import com.deloitte.assessment.model.Question;
import com.deloitte.assessment.model.Role;
import com.deloitte.assessment.model.RoleType;
import com.deloitte.assessment.model.TeamType;
import com.deloitte.assessment.model.WorkStream;
import com.deloitte.assessment.repository.CapabilityRepository;
import com.deloitte.assessment.repository.QuestionRepository;
import com.deloitte.assessment.repository.RoleRepository;
import com.deloitte.assessment.repository.RoleTypeRepository;

@Configuration
public class AppConfig {

	@Bean
	public CommandLineRunner loadQuestionTable(QuestionRepository qRepo, CapabilityRepository cRepo,
			RoleRepository rRepo, RoleTypeRepository rtRepo) {
		if (qRepo.count() == 0)
			return (args) -> {
				rtRepo.save(loadRoleTypes());
				Set<Question> questions = new HashSet<>();
				cRepo.save(loadCapabilities());
				// questions.addAll(readQuestionnaireExcel());
				// questions.addAll(readAssessmentExcel(cRepo));
				// questions.addAll(loadPortfolioQuestions(cRepo));
				questions.addAll(loadUpdatedQuestions(cRepo));
				qRepo.save(questions);
				rRepo.save(loadRoles(rtRepo));
			};
		else
			return (args) -> {

			};
	}

	private Set<RoleType> loadRoleTypes() {
		Set<RoleType> rtSet = new HashSet<>();
		Arrays.asList(WorkStream.values()).stream().forEach(ws -> {
			RoleType rt = new RoleType();
			rt.setWorkStream(ws);
			rt.setPortfolio(true);
			rt.setDelivery(false);
			rt.setProgram(false);
			rtSet.add(rt);
			rt = new RoleType();
			rt.setPortfolio(true);
			rt.setWorkStream(ws);
			rt.setDelivery(true);
			rt.setProgram(false);
			rtSet.add(rt);
			rt = new RoleType();
			rt.setWorkStream(ws);
			rt.setPortfolio(true);
			rt.setDelivery(false);
			rt.setProgram(true);
			rtSet.add(rt);
			rt = new RoleType();
			rt.setWorkStream(ws);
			rt.setPortfolio(true);
			rt.setDelivery(true);
			rt.setProgram(true);
			rtSet.add(rt);

			rt = new RoleType();
			rt.setWorkStream(ws);
			rt.setPortfolio(false);
			rt.setDelivery(false);
			rt.setProgram(false);
			rtSet.add(rt);
			rt = new RoleType();
			rt.setPortfolio(false);
			rt.setWorkStream(ws);
			rt.setDelivery(true);
			rt.setProgram(false);
			rtSet.add(rt);
			rt = new RoleType();
			rt.setWorkStream(ws);
			rt.setPortfolio(false);
			rt.setDelivery(false);
			rt.setProgram(true);
			rtSet.add(rt);
			rt = new RoleType();
			rt.setWorkStream(ws);
			rt.setPortfolio(false);
			rt.setDelivery(true);
			rt.setProgram(true);
			rtSet.add(rt);

		});
		return rtSet;
	}

	private Set<Question> loadUpdatedQuestions(CapabilityRepository cRepo) {
		Set<Question> questions = new HashSet<>();
		Sheet currentSheet = null;
		try (InputStream excelFile = getClass().getResourceAsStream("/UpdatedQuestions.xlsx")) {
			Workbook workbook = new XSSFWorkbook(excelFile);
			Iterator<Sheet> sheetIterator = workbook.iterator();

			while (sheetIterator.hasNext()) {
				currentSheet = sheetIterator.next();
				WorkStream ws = null;
				TeamType teamType = null;
				if ("Team Questions".equals(currentSheet.getSheetName())) {
					ws = Arrays.asList(WorkStream.values()).stream().filter(w -> w.getName().equals("Agile"))
							.findFirst().get();
					teamType = TeamType.Delivery;
				} else if ("Program Questions".equals(currentSheet.getSheetName())) {
					ws = Arrays.asList(WorkStream.values()).stream().filter(w -> w.getName().equals("Agile"))
							.findFirst().get();
					teamType = TeamType.ProgramManagement;
				} else if ("Portfolio Questions".equals(currentSheet.getSheetName())) {
					ws = Arrays.asList(WorkStream.values()).stream().filter(w -> w.getName().equals("Agile"))
							.findFirst().get();
					teamType = TeamType.PortfolioManagement;
				} else if ("Continuous Delivery Questions".equals(currentSheet.getSheetName())) {
					ws = Arrays.asList(WorkStream.values()).stream()
							.filter(w -> w.getName().equals("Continuous Delivery")).findFirst().get();
					teamType = TeamType.ContinuousDelivery;
				} 
				else
					continue;
				Iterator<Row> rowIterator = currentSheet.iterator();
				rowIterator.next();
				rowIterator.next();
				while (rowIterator.hasNext()) {
					Row currentRow = rowIterator.next();
					String questionText = currentRow.getCell(0).getStringCellValue();
//					System.out.println("ques text " + questionText);
					if(currentRow.getCell(3) == null)
						break;
					String cStr = currentRow.getCell(3).getStringCellValue();
//					System.out.println("cstr " + cStr);
					double weight;
					try {
						weight = currentRow.getCell(4).getNumericCellValue();
					} catch (IllegalStateException e) {
						continue;
					}
					Question question = new Question();
					if (!cStr.isEmpty()) {
						question.setText(questionText);
						question.setTeamType(teamType);
//						System.out.println("cstr " + cStr);
						CapabilityType capab = Arrays.asList(CapabilityType.values()).stream()
								.filter(c -> c.getName().equals(cStr)).findFirst().get();
						Capability c = cRepo.findByWorkStreamAndCapabilityType(ws, capab);
						question.setCapability(c);
						question.setWeight(weight);
						questions.add(question);
					}
				}

			}
		} catch (Exception e) {
			System.out.println("sheet name " + currentSheet.getSheetName());
			e.printStackTrace();
		}
		return questions;
	}

	private Set<Capability> loadCapabilities() {
		Set<Capability> capabilities = new HashSet<>();

		try (InputStream excelFile = getClass()
				.getResourceAsStream("/SunTrust Business Accelerator- Maturity Assessment v8.xlsx")) {

			Workbook workbook = new XSSFWorkbook(excelFile);

			Iterator<Sheet> sheetIterator = workbook.iterator();

			while (sheetIterator.hasNext()) {
				Sheet currentSheet = sheetIterator.next();
				String sheetName = currentSheet.getSheetName();
				if (sheetName.equals("Capabilities")) {
					Iterator<Row> rowIterator = currentSheet.iterator();
					rowIterator.next();
					while (rowIterator.hasNext()) {
						Row currentRow = rowIterator.next();
						String workStr = currentRow.getCell(0).getStringCellValue();
						if (workStr.isEmpty())
							continue;
						WorkStream workStream = Arrays.asList(WorkStream.values()).stream()
								.filter(ws -> ws.getName().equals(workStr)).findFirst().get();
						String cStr = currentRow.getCell(1).getStringCellValue();
						CapabilityType capab = Arrays.asList(CapabilityType.values()).stream()
								.filter(c -> c.getName().equals(cStr)).findFirst().get();
						Capability capability = new Capability();
						capability.setCapabilityType(capab);
						capability.setWorkStream(workStream);
						capabilities.add(capability);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return capabilities;
	}

	private List<Role> loadRoles(RoleTypeRepository rtRepo) {
		List<Role> roles = new ArrayList<>();
		try (InputStream excelFile = getClass().getResourceAsStream("/Roles.xlsx")) {
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet sheet = workbook.iterator().next();
			Iterator<Row> rowIterator = sheet.iterator();
			rowIterator.next();
			while (rowIterator.hasNext()) {
				Row currentRow = rowIterator.next();
				Role role = new Role();
				Set<RoleType> roleTypes = new HashSet<>();
				String roleName = currentRow.getCell(0).getStringCellValue();
				boolean agileProgram = 1 == currentRow.getCell(1).getNumericCellValue();
				boolean agileDeliv = 1 == currentRow.getCell(2).getNumericCellValue();
				boolean agilePort = 1 == currentRow.getCell(5).getNumericCellValue();
				RoleType rt;
				if (agileProgram || agileDeliv) {
					rt = rtRepo.findByWorkStreamAndProgramAndDeliveryAndPortfolio(WorkStream.Agile, agileProgram,
							agileDeliv, agilePort);
					roleTypes.add(rt);
					// rt.getRoles().add(role);
					// rtRepo.save(rt);
				}
				boolean devopsProgram = 1 == currentRow.getCell(3).getNumericCellValue();
				boolean devopsDeliv = 1 == currentRow.getCell(4).getNumericCellValue();
				boolean devPort = 1 == currentRow.getCell(5).getNumericCellValue();
				if (devopsProgram || devopsDeliv) {
					rt = rtRepo.findByWorkStreamAndProgramAndDeliveryAndPortfolio(WorkStream.ContinuousDelivery,
							devopsProgram, devopsDeliv, devPort);
					roleTypes.add(rt);
					// rt.getRoles().add(role);
					// rtRepo.save(rt);
				}
				role.setName(roleName);
				role.setRoleTypes(roleTypes);
				roles.add(role);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return roles;
	}
}

//private Set<Question> readAssessmentExcel(CapabilityRepository cRepo) {
//Set<Question> questions = new HashSet<>();
//try {
//	InputStream excelFile = getClass()
//			.getResourceAsStream("/SunTrust Business Accelerator- Maturity Assessment v8.xlsx");
//	Workbook workbook = new XSSFWorkbook(excelFile);
//
//	Iterator<Sheet> sheetIterator = workbook.iterator();
//
//	while (sheetIterator.hasNext()) {
//		Sheet currentSheet = sheetIterator.next();
//		String sheetName = currentSheet.getSheetName();
//		if (sheetName.equals("Agile")) {
//			WorkStream ws = Arrays.asList(WorkStream.values()).stream()
//					.filter(w -> w.getName().equals(sheetName)).findFirst().get();
//			Iterator<Row> rowIterator = currentSheet.iterator();
//			rowIterator.next();
//			while (rowIterator.hasNext()) {
//				Row currentRow = rowIterator.next();
//				String cellValue = currentRow.getCell(2).getStringCellValue();
//				String ttName = currentRow.getCell(4).getStringCellValue();
//				String cStr = currentRow.getCell(5).getStringCellValue();
//				double weight = currentRow.getCell(6).getNumericCellValue();
//				Question question = new Question();
//				if (!cellValue.isEmpty() && !ttName.isEmpty()) {
//					question.setText(cellValue);
//					TeamType teamType = null;
//					try {
//						teamType = Arrays.asList(TeamType.values()).stream()
//								.filter(t -> t.getName().equals(ttName)).findFirst().get();
//					} catch (Exception e) {
//						System.out.println(ttName);
//						throw e;
//					}
//					question.setTeamType(teamType);
//					CapabilityType capab = Arrays.asList(CapabilityType.values()).stream()
//							.filter(c -> c.getName().equals(cStr)).findFirst().get();
//					Capability c = cRepo.findByWorkStreamAndCapabilityType(ws, capab);
//					question.setCapability(c);
//					question.setWeight(weight);
//					questions.add(question);
//				}
//			}
//		} else if (sheetName.equals("Continuous Delivery")) {
//			WorkStream ws = Arrays.asList(WorkStream.values()).stream()
//					.filter(w -> w.getName().equals(sheetName)).findFirst().get();
//			Iterator<Row> rowIterator = currentSheet.iterator();
//			rowIterator.next();
//			while (rowIterator.hasNext()) {
//				Row currentRow = rowIterator.next();
//				String cellValue = currentRow.getCell(2).getStringCellValue();
//				String cStr = currentRow.getCell(5).getStringCellValue();
//				double weight = currentRow.getCell(7).getNumericCellValue();
//				Question question = new Question();
//				if (!cellValue.isEmpty() && !cStr.isEmpty()) {
//					CapabilityType capab = Arrays.asList(CapabilityType.values()).stream()
//							.filter(c -> c.getName().equals(cStr)).findFirst().get();
//					Capability c = cRepo.findByWorkStreamAndCapabilityType(ws, capab);
//					question.setCapability(c);
//					question.setText(cellValue);
//					question.setWeight(weight);
//					if (cStr.equals("Automated Deployment") || cStr.equals("Automated Functional Testing")
//							|| cStr.equals("Automated Non-Functional Testing"))
//						question.setTeamType(TeamType.Delivery);
//					else
//						question.setTeamType(TeamType.ContinuousDelivery);
//					questions.add(question);
//				}
//			}
//		}
//	}
//} catch (FileNotFoundException e) {
//	e.printStackTrace();
//} catch (IOException e) {
//	e.printStackTrace();
//} finally {
//
//}
//return questions;
//}

//private Set<Question> readQuestionnaireExcel() {
//Set<Question> questions = new HashSet<>();
//try {
//
//InputStream excelFile = getClass()
//		.getResourceAsStream("/SunTrust Questionnaire 20160516 Data Request Template.xlsm");
//Workbook workbook = new XSSFWorkbook(excelFile);
//
//Iterator<Sheet> sheetIterator = workbook.iterator();
//while (sheetIterator.hasNext()) {
//	Sheet currentSheet = sheetIterator.next();
//	String sheetName = currentSheet.getSheetName();
//	TeamType tt = null;
//	try {
//		tt = Arrays.asList(TeamType.values()).stream().filter(t -> t.getName().equals(sheetName))
//				.findFirst().get();
//	} catch (NoSuchElementException e) {
//		System.out.println("Exception sheetname is: " + sheetName);
//		continue;
//	}
//	if (tt != null) {
//		Iterator<Row> rowIterator = currentSheet.iterator();
//		rowIterator.next();
//		rowIterator.next();
//		while (rowIterator.hasNext()) {
//
//			Row currentRow = rowIterator.next();
//			String cellValue = currentRow.getCell(0).getStringCellValue();
//			if (!cellValue.isEmpty()) {
//				Question question = new Question();
//				question.setText(cellValue);
//				question.setTeamType(tt);
//				questions.add(question);
//			}
//
//		}
//	}
//}
//} catch (FileNotFoundException e) {
//e.printStackTrace();
//} catch (IOException e) {
//e.printStackTrace();
//} finally {
//
//}
//return questions;
//}

//private Set<Question> loadPortfolioQuestions(CapabilityRepository cRepo) {
//	Set<Question> questions = new HashSet<>();
//
//	try (InputStream excelFile = getClass().getResourceAsStream("/Agile_Assessment_Portfolio.xlsx")) {
//		Workbook workbook = new XSSFWorkbook(excelFile);
//		Iterator<Sheet> sheetIterator = workbook.iterator();
//		Sheet sheet = null;
//		while (sheetIterator.hasNext()) {
//			sheet = sheetIterator.next();
//			if ("Agile_Portfolio (2)".equals(sheet.getSheetName()))
//				break;
//		}
//		Iterator<Row> rowIterator = sheet.iterator();
//		rowIterator.next();
//		while (rowIterator.hasNext()) {
//			Row currentRow = rowIterator.next();
//			if (currentRow.getCell(0) == null)
//				break;
//			String questionName = currentRow.getCell(0).getStringCellValue();
//			String capName = currentRow.getCell(3).getStringCellValue();
//			CapabilityType capab = Arrays.asList(CapabilityType.values()).stream()
//					.filter(c -> c.getName().equals(capName)).findFirst().get();
//			Capability cap = cRepo.findByCapabilityType(capab).stream().findFirst().get();
//			Question question = new Question();
//			question.setText(questionName);
//			question.setCapability(cap);
//			question.setTeamType(TeamType.PortfolioManagement);
//			questions.add(question);
//		}
//	} catch (Exception e) {
//		e.printStackTrace();
//	}
//	return questions;
//}
