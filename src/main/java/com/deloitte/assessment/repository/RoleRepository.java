package com.deloitte.assessment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deloitte.assessment.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{

	public Role findByName(String name);
}
