package com.deloitte.assessment.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.deloitte.assessment.model.Capability;
import com.deloitte.assessment.model.Question;
import com.deloitte.assessment.model.TeamType;

public interface QuestionRepository extends JpaRepository<Question, Long> {

	public Set<Question> findByCapability(Capability capability);

	@Query("Select q from Question q where q.capability in :capabilities")
	public Set<Question> findByCapabilities(@Param("capabilities") Set<Capability> capabilities);

	@Query("Select q from Question q where q.teamType = :tt and q.capability in :capabilities")
	public Set<Question> findByTeamTypeAndCapabilities(@Param("tt") TeamType tt,
			@Param("capabilities") Set<Capability> capabilities);

	public Set<Question> findByTeamType(TeamType tt);
}
