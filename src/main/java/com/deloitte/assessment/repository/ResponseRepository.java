package com.deloitte.assessment.repository;

import java.util.Collection;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deloitte.assessment.model.Response;
import com.deloitte.assessment.model.Survey;

public interface ResponseRepository extends JpaRepository<Response, Long> {

//	public Set<Response> findByEmployee(Employee employee);

//	@Query("Select res from Response res inner join Employee e inner join Role r where e.role = :role and e.segment = :segment")
//	@Query("Select res from Response res where res.employee.role = ?1 and res.employee.segment = ?2")
//	public Set<Response> findByRole(Role role, Segment segment);

//	@Query("Select res from Response res inner join Employee e where e.segment = :segment")
//	@Query("Select res from Response res where res.employee.segment = ?1 and res.question.teamType = ?2")
//	public Set<Response> findByTeamType(Segment segment, TeamType tt);
	
	public Set<Response> findBySurveyIn(Collection<Survey> survey);
}
