package com.deloitte.assessment.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.deloitte.assessment.model.Capability;
import com.deloitte.assessment.model.CapabilityType;
import com.deloitte.assessment.model.WorkStream;

public interface CapabilityRepository extends JpaRepository<Capability, Long>{

	public Capability findByWorkStreamAndCapabilityType(WorkStream ws, CapabilityType c);
	
	@Query("Select c from Capability c where c.workStream in :ws")
	public Set<Capability> findByWorkStreams(@Param("ws") Set<WorkStream> ws);
	
	public Set<Capability> findByWorkStream(WorkStream ws);
	
	public Set<Capability> findByCapabilityType(CapabilityType cType);
}
