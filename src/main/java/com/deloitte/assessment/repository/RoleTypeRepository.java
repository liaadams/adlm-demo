package com.deloitte.assessment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deloitte.assessment.model.RoleType;
import com.deloitte.assessment.model.WorkStream;

public interface RoleTypeRepository extends JpaRepository<RoleType, Long> {

	public RoleType findByWorkStreamAndProgramAndDelivery(WorkStream ws, boolean program, boolean delivery);

	public RoleType findByWorkStreamAndProgramAndDeliveryAndPortfolio(WorkStream ws, boolean program, boolean delivery,
			boolean portfolio);
}
