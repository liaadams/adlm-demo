package com.deloitte.assessment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deloitte.assessment.model.Organization;

public interface OrganizationRepository extends JpaRepository<Organization, Long> {

	public Organization findByName(String name);
}
