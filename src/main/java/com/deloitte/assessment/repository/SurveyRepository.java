package com.deloitte.assessment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deloitte.assessment.model.Employee;
import com.deloitte.assessment.model.Survey;

public interface SurveyRepository extends JpaRepository<Survey, String>{

	public List<Survey> findByEmployee(Employee employee);
	
	public Survey findByEmployeeAndRoleName(Employee employee, String roleName);
	
}
