package com.deloitte.assessment.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deloitte.assessment.model.Employee;
import com.deloitte.assessment.model.Segment;

public interface EmployeeRepository extends JpaRepository<Employee, String>{
	
	public Employee findByEmailAndSegment(String email, Segment segment);
	
	public Collection<Employee> findBySegment(Segment segment);

}
