package com.deloitte.assessment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deloitte.assessment.model.Segment;

public interface SegmentRepository extends JpaRepository<Segment, Long>{
	
	public Segment findByName(String name);

}
