package com.deloitte.assessment.service;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.deloitte.assessment.model.Employee;
import com.deloitte.assessment.model.Role;
import com.deloitte.assessment.model.Survey;

@Component
public class EmailService {

	@Value("${mail.host}")
	private String host;

	@Value("${mail.port}")
	private String port;

	@Value("${mail.from}")
	private String from;

	private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

	public void sendQuestionnaireLink(String email, List<Survey> surveys) throws MessagingException {
		Properties props = new Properties();
		props.setProperty("mail.smtp.host", host);
		props.setProperty("mail.smtp.port", port);
		Session session = Session.getDefaultInstance(props);

		for (Survey s : surveys) {
			// employees.forEach(e -> {
			Message message = new MimeMessage(session);
			try {
				String link = null;
				message.setFrom(new InternetAddress(from));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
				message.setSubject("Survey Invitation");
				for (Survey survey : surveys) {
					link = "http://azealadvwb01.dir.dcloudservice.com/survey/" + survey.getSurvId();
					message.setText("Please follow link to survey " + link);
				}
				// if (role.getName().equals("Portfolio Admin")) {
				// link =
				// "http://azealadvwb01.dir.dcloudservice.com/survey/team/Portfolio%20Level/"
				// + e.getEmpId();
				// } else if (role.getName().equals("Program Admin")) {
				// link =
				// "http://azealadvwb01.dir.dcloudservice.com/survey/team/Program%20Level/"
				// + e.getEmpId();
				// } else if (role.getName().equals("Team Admin")) {
				// link =
				// "http://azealadvwb01.dir.dcloudservice.com/survey/team/Team%20Level/"
				// + e.getEmpId();
				// } else if (role.getName().equals("Continuous Delivery
				// Admin"))
				// link =
				// "http://azealadvwb01.dir.dcloudservice.com/survey/team/" +
				// "Continuous%20Delivery/"
				// + e.getEmpId();
				// else {
				// link = "http://azealadvwb01.dir.dcloudservice.com/survey/" +
				// e.getEmpId();
				// }
			} catch (MessagingException ex) {
				// ex.printStackTrace();
				logger.error(ex.getMessage());
				// throw ex;
			}
			Transport.send(message);
		}
		// });

	}
}
