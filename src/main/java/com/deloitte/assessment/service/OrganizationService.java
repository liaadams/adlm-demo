package com.deloitte.assessment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deloitte.assessment.model.Organization;
import com.deloitte.assessment.repository.OrganizationRepository;

@Component
public class OrganizationService {

	@Autowired
	private OrganizationRepository organizationRepo;
	
	public Organization addOrganization(Organization organization) {
		return organizationRepo.save(organization);
	}
	
	public Organization getOrganization(Organization organization) {
		return organizationRepo.findByName(organization.getName());
	}
	
	public Organization getOrganization(String name) {
		return organizationRepo.findByName(name);
	}
	
	public void updateOrganization(Organization organization) {
		organizationRepo.save(organization);
	}
	
	public List<Organization> getOrganizations() {
		return organizationRepo.findAll();
	}
}
