package com.deloitte.assessment.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deloitte.assessment.model.Capability;
import com.deloitte.assessment.model.CapabilityType;
import com.deloitte.assessment.model.WorkStream;
import com.deloitte.assessment.repository.CapabilityRepository;

@Component
public class CapabilityService {

	@Autowired
	private CapabilityRepository cRepo;
	
	public Set<Capability> getByWorkStreams(Set<WorkStream> ws) {
		return cRepo.findByWorkStreams(ws);
	}
	
	public Set<Capability> getByWorkStream(WorkStream ws) {
		return cRepo.findByWorkStream(ws);
	}
	
	public Set<Capability> getByCapabilityType(CapabilityType cType) {
		return cRepo.findByCapabilityType(cType);
	}
	
	public List<Capability> getAll() {
		return cRepo.findAll();
	}
	
	public void addCapability(Capability capability){
		cRepo.save(capability);
	}
	
	public void update(Capability cap) {
		cRepo.save(cap);
	}
	
}
