package com.deloitte.assessment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deloitte.assessment.model.Segment;
import com.deloitte.assessment.repository.SegmentRepository;

@Component
public class SegmentService {

	@Autowired
	private SegmentRepository segmentRepo;
	
	public Segment addSegment(Segment segment) {
		return segmentRepo.save(segment);
	}
	
	public Segment getSegment(Long id) {
		return segmentRepo.findOne(id);
	}
	
	public Segment getSegment(Segment seg) {
		return segmentRepo.findByName(seg.getName());
	}
}
