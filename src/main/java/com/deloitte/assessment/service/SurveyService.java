package com.deloitte.assessment.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deloitte.assessment.model.Employee;
import com.deloitte.assessment.model.Role;
import com.deloitte.assessment.model.Survey;
import com.deloitte.assessment.repository.EmployeeRepository;
import com.deloitte.assessment.repository.SurveyRepository;

@Component
public class SurveyService {

	@Autowired
	private SurveyRepository surveyRepository;
	
	@Autowired
	private EmployeeRepository empRepo;

	public Survey getSurvey(String uuid) {
		return surveyRepository.findOne(uuid);
	}

//	public boolean empHasSurvey(Employee emp) {
//		List<Survey> surveys = getSurveys(emp);
//		for (Survey survey : surveys)
//			emp.getRoles().stream().filter(r -> r.getName().equals(survey.getRoleName())).findAny().get();
//	}

	public List<Survey> getSurveys(Employee emp) {
		List<Survey> surveys = new ArrayList<>();
		Survey survey = null;
		for (Role role : emp.getRoles()) {
			survey = surveyRepository.findByEmployeeAndRoleName(emp, role.getName());
			if (survey != null)
				surveys.add(survey);
		}
		return surveys;
	}

	// explain service to trigger build
	public Survey addSurvey(Employee emp, String roleName) {
		if(emp.getSurveys() == null)
			emp.setSurveys(new ArrayList<Survey>());
		Survey survey = new Survey();
		survey.setEmployee(emp);
		emp.getSurveys().add(survey);
		survey.setRoleName(roleName);
		empRepo.save(emp);
		return survey;
	}
	
	public List<Survey> addSurveys(Employee emp, Collection<String> roleNames) {
		if(emp.getSurveys() == null)
			emp.setSurveys(new ArrayList<Survey>());
		roleNames.forEach(r -> {
			Survey survey = new Survey();
			survey.setEmployee(emp);
			emp.getSurveys().add(survey);
			survey.setRoleName(r);
			emp.getSurveys().add(survey);
		});
		empRepo.save(emp);
		return emp.getSurveys();
	}
	
	public List<Survey> addSurveys(Employee emp) {
		if(emp.getSurveys() == null)
			emp.setSurveys(new ArrayList<Survey>());
		emp.getRoles().forEach(r -> {
			Survey survey = new Survey();
			survey.setEmployee(emp);
			emp.getSurveys().add(survey);
			survey.setRoleName(r.getName());
		});
		empRepo.save(emp);
		return emp.getSurveys();
	}

	public void updateSurvey(Survey survey) {
		surveyRepository.save(survey);
	}
}
