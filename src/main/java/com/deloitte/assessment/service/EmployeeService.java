package com.deloitte.assessment.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deloitte.assessment.model.Employee;
import com.deloitte.assessment.model.Segment;
import com.deloitte.assessment.repository.EmployeeRepository;

@Component
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	public List<Employee> addEmployees(Set<Employee> employees) {
		return employeeRepository.save(employees);
	}
	
	public Employee addEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}
	
	public Employee getEmployee(String id) {
		return employeeRepository.findOne(id);
	}
	
	public Collection<Employee> getBySegment(Segment seg) {
		return employeeRepository.findBySegment(seg);
	}
	
	public Employee getEmployee(String email, Segment segment) {
		return employeeRepository.findByEmailAndSegment(email, segment);
	}
	
	public void updateEmployee(Employee employee) {
		employeeRepository.save(employee);
	}
	
}
