package com.deloitte.assessment.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deloitte.assessment.model.Organization;
import com.deloitte.assessment.model.Response;
import com.deloitte.assessment.model.Role;
import com.deloitte.assessment.model.Segment;
import com.deloitte.assessment.model.Survey;
import com.deloitte.assessment.model.TeamType;
import com.deloitte.assessment.repository.ResponseRepository;

@Component
public class ResponseService {

	@Autowired
	private ResponseRepository rRepo;

	@Autowired
	private RoleService roleService;

	// public Set<Response> getByEmployee(Employee employee) {
	// return rRepo.findByEmployee(employee);
	// }

	public List<Response> saveResponses(List<Response> responses) {
		return rRepo.save(responses);
	}

	public Workbook getByRole(String name, Segment segment) {
		Role role = roleService.getRoleByName(name);
		// Set<Response> responses = rRepo.findByRole(role, segment);
		Set<Response> responses = null;
		return getExcelFile(null);
	}

	public Workbook getByTeam(TeamType tt, Segment segment) {
		// Set<Response> responses = rRepo.findBySegment(segment);
		// return responses.stream().filter(r ->
		// r.getQuestion().getTeamType().equals(tt)).collect(Collectors.toSet());
		// Set<Response> responses = rRepo.findByTeamType(segment, tt);
		Set<Response> responses = null;
		return getExcelFile(null);
	}

	public Workbook getByOrganization(Organization org) {
		Set<Segment> segs = org.getSegments();
		Map<Segment, Set<Response>> responseMap = new HashMap<>();
		for (Segment seg : segs) {
			List<Survey> surveys = new ArrayList<>();
			seg.getEmployees().stream().forEach(e -> surveys.addAll(e.getSurveys()));
			Set<Response> responses = rRepo.findBySurveyIn(surveys);
			responseMap.put(seg, responses);
		}
		// org.getSegments().stream().forEach(s ->
		// s.getEmployees().stream().forEach(e ->
		// surveys.addAll(e.getSurveys())));
		return getExcelFile(responseMap);
	}

	private Workbook getExcelFile(Map<Segment, Set<Response>> responseMap) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		for (Segment seg : responseMap.keySet()) {
			Set<Response> responses = responseMap.get(seg);
			XSSFSheet sheet = workbook.createSheet(seg.getName());
			int rowNum = 0, colNum = 0;
			Row row = sheet.createRow(rowNum++);
			Cell cell = row.createCell(colNum++);
			cell.setCellValue("Question");
			cell = row.createCell(colNum++);
			cell.setCellValue("Response");
			cell = row.createCell(colNum++);
			cell.setCellValue("Employee Name");
			cell = row.createCell(colNum++);
			cell.setCellValue("Employee Role");
			cell = row.createCell(colNum++);
			cell.setCellValue("Weight");
			cell = row.createCell(colNum++);
			cell.setCellValue("Capability");
			for (Response r : responses) {
				colNum = 0;
				row = sheet.createRow(rowNum++);
				cell = row.createCell(colNum++);
				cell.setCellValue(r.getQuestion().getText());
				cell = row.createCell(colNum++);
				cell.setCellValue(r.getContent());
				cell = row.createCell(colNum++);
				cell.setCellValue(
						r.getSurvey().getEmployee().getFirstName() + " " + r.getSurvey().getEmployee().getLastName());
				cell = row.createCell(colNum++);
				cell.setCellValue(r.getSurvey().getRoleName());
				cell = row.createCell(colNum++);
				cell.setCellValue(r.getQuestion().getWeight());
				cell = row.createCell(colNum++);
				cell.setCellValue(r.getQuestion().getCapability().getCapabilityType().name());
			}
		}
		return workbook;
	}

	public void delete(Response response) {
		rRepo.delete(response);
	}

}
