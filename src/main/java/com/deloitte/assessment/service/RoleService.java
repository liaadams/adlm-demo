package com.deloitte.assessment.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deloitte.assessment.model.Role;
import com.deloitte.assessment.repository.RoleRepository;

@Component
public class RoleService {

	@Autowired
	private RoleRepository roleRepo;
	
	public Role getRole(Role role) {
		return roleRepo.findOne(role.getId());
	}
	
	public Collection<Role> getRoles(Collection<Role> roles) {
		List<Long> ids = new ArrayList<Long>();
		for(Role r : roles)
			ids.add(r.getId());
		return roleRepo.findAll(ids);
	}
	
	public List<Role> getRoles() {
		return roleRepo.findAll();
	}
	
	public void updateRole(Role role) {
		roleRepo.save(role);
	}
	
	public Role getRoleByName(String name) {
		return roleRepo.findByName(name);
	}
}
