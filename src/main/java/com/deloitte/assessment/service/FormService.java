package com.deloitte.assessment.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deloitte.assessment.model.Employee;
import com.deloitte.assessment.model.Organization;
import com.deloitte.assessment.model.Role;
import com.deloitte.assessment.model.Segment;
import com.deloitte.assessment.model.Survey;

@Component
public class FormService {

	@Autowired
	private OrganizationService orgService;

	@Autowired
	private SegmentService segService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private SurveyService surveyService;

	@Autowired
	private RoleService roleService;
	
	@Autowired
	private EmployeeService empService;

	public void insertData(Organization org, Map<String, Object> model) throws MessagingException {
		Organization org2 = orgService.getOrganization(org);
		Segment seg = org.getSegments().stream().findFirst().get();
		Segment seg2 = segService.getSegment(seg);
		Set<Employee> empsToAdd = new HashSet<>();
		if (org2 != null) {
			org = org2;
			List<String> errors = new ArrayList<>();
			if (seg2 != null) {	
				Collection<Employee> dbEmps = seg2.getEmployees();
				Collection<Employee> formEmps = seg.getEmployees();
				for (Employee e : formEmps) {
					if(!dbEmps.contains(e)) {
						seg2.getEmployees().add(e);
						e.setSegment(seg2);
						empsToAdd.add(e);
						continue;
					}
					Employee dbEmp = empService.getEmployee(e.getEmail(), seg2);
					List<Survey> surveys = surveyService.getSurveys(dbEmp);
					List<String> surveyRoles = new ArrayList<>();
					List<String> empRoles = new ArrayList<>();
					surveys.forEach(s -> {
						surveyRoles.add(s.getRoleName());
					});
					e.getRoles().forEach(r -> {
						empRoles.add(r.getName());
					});
					List<String> intersect = empRoles.stream().filter(surveyRoles::contains)
							.collect(Collectors.toList());
					for (String s : intersect) 
						errors.add("Survey already created for " + e.getEmail() 
								+ " for this organization, LOB and " + s);
					
					empRoles.removeAll(surveyRoles);
					if (!empRoles.isEmpty()) {
						for(String s : empRoles) {
							Role role = roleService.getRoleByName(s);
							e.getRoles().add(role);
							role.getEmployees().add(e);
							empService.updateEmployee(e);
						}
						List<Survey> newSurveys = surveyService.addSurveys(e, empRoles);
						emailService.sendQuestionnaireLink(e.getEmail(), newSurveys);
					}
				}
				model.put("surveyErrors", errors);
			} else {
				for (Employee e : seg.getEmployees()) {
					setRoles(e);
				}
				seg.setOrganization(org2);
				org2.getSegments().add(seg);
				orgService.updateOrganization(org2);
//				seg = segService.addSegment(seg);
				seg = segService.getSegment(seg);
				createSurveys(seg.getEmployees());
			}
		} else {
			for (Segment s : org.getSegments())
				for (Employee e : s.getEmployees()) {
					setRoles(e);
				}
			org = orgService.addOrganization(org);
			for (Segment s : org.getSegments())
				createSurveys(s.getEmployees());
		}
		for (Employee e : empsToAdd) {
			setRoles(e);
			e = empService.addEmployee(e);
			List<Survey> surveys = surveyService.addSurveys(e);
			emailService.sendQuestionnaireLink(e.getEmail(), surveys);
		}
	}
	
	private void setRoles(Employee e) {
		Collection<Role> dbRoles = roleService.getRoles(e.getRoles());
		e.setRoles(dbRoles);
		for (Role r : dbRoles)
			r.getEmployees().add(e);
	}
	
	private void createSurveys(Collection<Employee> emps) throws MessagingException {
		for(Employee e : emps) {
			List<Survey> surveys = surveyService.addSurveys(e);
			emailService.sendQuestionnaireLink(e.getEmail(), surveys);
		}
	}
}
