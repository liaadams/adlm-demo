package com.deloitte.assessment.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.deloitte.assessment.model.Capability;
import com.deloitte.assessment.model.Question;
import com.deloitte.assessment.model.Role;
import com.deloitte.assessment.model.RoleType;
import com.deloitte.assessment.model.Survey;
import com.deloitte.assessment.model.TeamType;
import com.deloitte.assessment.repository.QuestionRepository;

@Component
public class QuestionService {

	@Autowired
	private QuestionRepository qRepo;

	@Autowired
	private CapabilityService cService;

	@Autowired
	private RoleService rService;

	@Autowired
	private EmployeeService eService;

	@Autowired
	private ResponseService resService;
	
	@Autowired
	private SurveyService survService;

	public void updateQuestion(Question q) {
		qRepo.save(q);
	}
	
	public Page<Question> getQuestionPage(Map<String, Object> params) {
		Pageable page = null;
		if(params.containsKey("limit")){
			int limit = Integer.parseInt((String)params.get("limit"));
			int offset = Integer.parseInt((String)params.get("offset"));
			page = new PageRequest(offset, limit);
		}
		return qRepo.findAll(page);
	}

	public List<Question> getQuestions(Map<String, Object> params) {
		List<Question> questions = new ArrayList<>();
		if(params.containsKey("survId")) {
			String survId = (String) params.get("survId");
			Survey survey = survService.getSurvey(survId);
			params.put("role", survey.getRoleName());
		}

		if (params.containsKey("role")) {
			String roleName = (String) params.get("role");
			if (roleName.equals("Portfolio Admin") || roleName.equals("Program Admin")
					|| roleName.equals("Team Level")) {
				String newName = roleName.replaceAll("Admin", "Level");
				TeamType tType = Arrays.asList(TeamType.values()).stream().filter(t -> t.getName().equals(newName))
						.findFirst().get();
				questions.addAll(qRepo.findByTeamType(tType));
			} else {
				Role role = rService.getRoleByName(roleName);
				addQuestionsFromRole(role, questions);
			}
		}
		
		if(params.isEmpty())
			questions.addAll(qRepo.findAll());
		return questions;
	}

	private void addQuestionsFromRole(Role role, List<Question> questions) {
		for (RoleType rt : role.getRoleTypes()) {
			// role.getRoleTypes().forEach(rt -> {
			Set<Capability> caps = cService.getByWorkStream(rt.getWorkStream());
			if (rt.isDelivery()) {
				TeamType teamType = Arrays.asList(TeamType.values()).stream()
						.filter(t -> t.getName().equals("Team Level")).findFirst().get();
				questions.addAll(qRepo.findByTeamTypeAndCapabilities(teamType, caps));
			}
			if (rt.isProgram()) {
				TeamType teamType = Arrays.asList(TeamType.values()).stream()
						.filter(t -> t.getName().equals("Program Level")).findFirst().get();
				questions.addAll(qRepo.findByTeamTypeAndCapabilities(teamType, caps));
			}
			if (rt.isPortfolio()) {
				TeamType teamType = Arrays.asList(TeamType.values()).stream()
						.filter(t -> t.getName().equals("Portfolio Level")).findFirst().get();
				questions.addAll(qRepo.findByTeamTypeAndCapabilities(teamType, caps));
			}
			// });
		}
	}

}
