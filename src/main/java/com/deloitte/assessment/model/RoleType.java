package com.deloitte.assessment.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table
@Data
@EqualsAndHashCode(exclude="roles")
public class RoleType {

	@Id
	@GeneratedValue
	@Column
	private int id;
	
	@Enumerated(EnumType.STRING)
	private WorkStream workStream;
	
	@Column
	private boolean program;
	
	@Column
	private boolean delivery;
	
	@Column
	private boolean portfolio;
	
	@ManyToMany(mappedBy = "roleTypes")
	@Fetch(FetchMode.JOIN)
	@JsonBackReference(value="role-rt")
	private Set<Role> roles;
}
