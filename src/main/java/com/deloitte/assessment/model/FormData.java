package com.deloitte.assessment.model;

import java.util.Set;

import lombok.Data;

@Data
public class FormData {

	private Organization organization;
	private Segment segment;
	private Set<Employee> employees;
}
