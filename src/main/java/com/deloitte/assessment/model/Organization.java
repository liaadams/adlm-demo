package com.deloitte.assessment.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Entity
@Table
@Data
public class Organization implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column
	private int id;
	
	@Column
	private String name;
	
	@Column
	private boolean portfolioSurvey;
	
	@Column
	private boolean programSurvey;
	
	@Column
	private boolean teamSurvey;
	
	@Column
	private boolean contDelivSurvey;
	
	@OneToMany(cascade = {CascadeType.ALL})
	@JsonManagedReference(value="org-segs")
	private Set<Segment> segments;
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object other) {
		Organization org = (Organization)other;
		if(this.name.equals(org.getName()))
			return true;
		else
			return false;
	}
}
