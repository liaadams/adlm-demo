package com.deloitte.assessment.model;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CapabilityTypeSerializer extends JsonSerializer<CapabilityType> {

	@Override
	public void serialize(CapabilityType value, JsonGenerator generator, SerializerProvider arg2) throws IOException {
		generator.writeStartObject();
		generator.writeFieldName("capabilityType");
		generator.writeString(value.getName());
		generator.writeEndObject();
	}

}
