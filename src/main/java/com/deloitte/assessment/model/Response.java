package com.deloitte.assessment.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table
@Data
//@EqualsAndHashCode(exclude = "employee")
public class Response {

	@Id
	@GeneratedValue
	@Column
	private int id;

	@Column
	private String content;

	@ManyToOne
//	@JsonIgnore
	@JsonBackReference(value = "ques-resps")
	private Question question;
	
	@ManyToOne
	@JsonBackReference(value = "surv-resps")
	private Survey survey;
	
//	@ManyToOne
////	@JsonIgnore
//	@JsonBackReference(value = "emp-resps")
//	private Employee employee;

//	@Override
//	public boolean equals(Object obj) {
//		Response other = (Response) obj;
//		return this.getQuestion().equals(other.getQuestion()) && this.getEmployee().equals(other.getEmployee());
//	}
//	
//	@Override 
//	public int hashCode() {
//		if(this.getQuestion() == null || this.getEmployee() == null)
//			return super.hashCode();
//		return this.getQuestion().getId().hashCode() * this.getEmployee().getEmpId().hashCode();
//	}

}
