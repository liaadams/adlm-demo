package com.deloitte.assessment.model;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum WorkStream {

	Agile("Agile"), Cloud("Cloud"), ContinuousDelivery("Continuous Delivery"), DesignThinking("Design Thinking"),
	ModularArchitecture("Modular Architecture");
	
	private String name;
	
	private WorkStream(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	@JsonCreator
    public static WorkStream forValue(String value) {
        return WorkStream.valueOf(value);
    }
}
