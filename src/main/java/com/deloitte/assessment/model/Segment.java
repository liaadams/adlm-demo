package com.deloitte.assessment.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table
@Data
//@EqualsAndHashCode(exclude="organization")
public class Segment implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column
	private Long id;
	
	@Column
	private String name;
	
	@ManyToOne
	@JsonBackReference(value="org-segs")
	private Organization organization;
	
	@OneToMany(cascade = {CascadeType.ALL})
	@JsonManagedReference(value="seg-emps")
	private Set<Employee> employees;
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object other) {
		Segment seg = (Segment)other;
		if(this.getOrganization().equals(seg.getOrganization()) && this.getName().equals(seg.getName()))
			return true;
		else
			return false;
	}
}
