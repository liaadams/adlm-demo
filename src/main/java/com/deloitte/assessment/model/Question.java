package com.deloitte.assessment.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Entity
@Table
@Data
public class Question {

	@Id
	@GeneratedValue
	@Column
	private Long id;
	
	@Column
	private String text;
	
	@Column
	private double weight;
	
	@Enumerated(EnumType.STRING)
	private TeamType teamType;
	
//	@Column
//	private String deliveryOrProgram;
	
//	@Column
//	private WorkStream workStream;
	
	@ManyToOne
//	@JoinTable(name="capability_questions", joinColumns=@JoinColumn(name="qId"), inverseJoinColumns=@JoinColumn(name="cId"))
//	@JsonManagedReference(value="cap-ques")
	private Capability capability;
	
	@OneToMany(cascade = {CascadeType.ALL})
	@JsonManagedReference(value="ques-resps")
	private Set<Response> response;
	
	@Override
	public boolean equals(Object obj) {
		Question other = (Question)obj;
		return this.id == other.id;
	}
	
}
