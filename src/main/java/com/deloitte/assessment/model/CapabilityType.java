package com.deloitte.assessment.model;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum CapabilityType {

	RolesAndTeamStructure("Roles and Team Structure"),	AgilePractices("Agile Practices"), 
	ApiBasedInfrastructureAccess("API Based Infrastructure Access"), 
	APIs("API's"), ApplicationConfigurationManagement("Application Configuration Management"), 
	AutomatedBuild("Automated Build"), AutomatedDeployment("Automated Deployment"), 
	AutomatedFunctionalTesting("Automated Functional Testing"), 
	AutomatedNonFunctionalTesting("Automated Non-Functional Testing"), AutomatedUnitTesting("Automated Unit Testing"),
	BlueprintsAndPattern("Blueprints & Pattern"), BuildDependencyManagement("Build Dependency Management"), 
	CodeScanning("Code Scanning"), Communications("Communications"), Containers("Containers"), Data("Data"),
	DataManagement("Data Management"), DataVirtualizationManagement("Data Virtualization/Management"),
	DeveloperAccessToCloudServices("Developer Access to Cloud Services"), EnvironmentConfigurationManagement("Environment Configuration Management"),
	ExperiencePlanning("Experience Planning"), FieldResearch("Field Research"), Governance("Governance"),
	HiringAndStaffing("Hiring and Staffing"), InnovationModeling("Innovation Modeling"), MetricsAndKpis("Metrics & KPIs"),
	Prototyping("Prototyping"), PublicCloud("Public Cloud"), ReleaseOrchestration("Release Orchestration"),
	RuntimeDependencyManagement("Runtime Dependency Management"), SourceCodeControl("Source Code Control"),
	Standards("Standards"), Tools("Tools"), Training("Training"), UserTestingAndUsability("User Testing & Usability"),
	UXWorkshopsAndDesignThinkingSession("UX Workshops & Design Thinking Sessions"),
	VisualDesignMockUps("Visual Design/ Mock-ups"), WireframingAndInformationArch("WireFraming and Information Arch");

	private String name;

	private CapabilityType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return name;
	}
	
	@JsonCreator
    public static CapabilityType forValue(String value) {
        return Arrays.asList(CapabilityType.values()).stream()
				.filter(c -> c.getName().equals(value)).findFirst().get();
    }
	
}
