package com.deloitte.assessment.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Entity
@Table
@Data
//@EqualsAndHashCode(exclude="segment")
public class Employee implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator="uuid")
	@GenericGenerator(name="uuid", strategy="uuid2")
	@Column
	private String empId;
		
//	@Column
//	private int employee_id;
	
	@Column
	private String firstName;
	
	@Column
	private String lastName;
	
	@Column
	private String email;
	
	@Enumerated(EnumType.STRING)
	private TeamType teamType;
	
//	@Column
//	private boolean isSurveyComplete;
	
	@ManyToOne
	@JsonBackReference(value="seg-emps")
	private Segment segment;
	
//	@OneToMany(cascade = {CascadeType.ALL})
//	@JsonManagedReference(value="emp-resps")
//	private Set<Response> responses;
	
	@ManyToMany(mappedBy = "employees", cascade = {CascadeType.ALL})
//	@JoinTable(name="role_employees", joinColumns=@JoinColumn(name="empId"), inverseJoinColumns=@JoinColumn(name="id"))
//	@JsonManagedReference(value="role-emps")
	private Collection<Role> roles;
	
	@OneToMany(cascade = {CascadeType.ALL})
	@JsonManagedReference(value="emp-survs")
	private List<Survey> surveys;
	
	@Override
	public boolean equals(Object obj) {
		Employee other = (Employee)obj;
		if(this.getSegment().equals(other.getSegment()) && this.getEmail().equals(other.getEmail()))
			return true;
		else
			return false;
	}
	
	@Override
	public int hashCode() {
		return this.firstName.hashCode() * this.lastName.hashCode() * this.email.hashCode();
	}
}
