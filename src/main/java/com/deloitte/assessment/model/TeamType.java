package com.deloitte.assessment.model;

public enum TeamType {

	PortfolioManagement("Portfolio Level"), ProgramManagement("Program Level"), ProductManagement(
			"Product Management"), Delivery("Team Level"), ContinuousDelivery("Continuous Delivery"), ReleaseManagement(
					"Release Management"), OperationsManagement("Operations Management"), General("General");
	
//	PortfolioManagement("Portfolio Level"), ProgramManagement("Program Level"),
//	Delivery("Team Level"), ContinuousDelivery("Continuous Delivery");
	
	private String name;
	private TeamType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
