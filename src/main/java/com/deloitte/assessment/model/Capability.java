package com.deloitte.assessment.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;

@Entity
@Table
@Data
public class Capability {

	@Id
	@Column
	@GeneratedValue
	private Long id;
	
	@Enumerated(EnumType.STRING)
	private CapabilityType capabilityType;
	
	@Enumerated(EnumType.STRING)
	private WorkStream workStream;
	
	@OneToMany
	@Fetch(FetchMode.JOIN)
//	@JoinColumn(name="id")
	@JsonBackReference(value="cap-ques")
	private Set<Question> questions;
	
//	@ManyToMany
////	@JsonBackReference(value="role-caps")
//	private Set<Role> role;
}
