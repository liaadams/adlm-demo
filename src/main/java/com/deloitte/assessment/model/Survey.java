package com.deloitte.assessment.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Entity
@Table
@Data
public class Survey implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator="uuid")
	@GenericGenerator(name="uuid", strategy="uuid2")
	@Column
	private String survId;
	
	@ManyToOne
	@JsonBackReference(value="emp-survs")
	private Employee employee;
//	
//	@ManyToOne
//	@JsonBackReference(value="role-survs")
//	private Role role;
//	
//	@Enumerated(EnumType.STRING)
//	private TeamType teamType;
	
//	@ManyToMany
//	@JoinTable(name="surv_ques", joinColumns=@JoinColumn(name="survId"), inverseJoinColumns=@JoinColumn(name="id"))
//	@JsonBackReference(value="surv-ques")
//	private List<Question> questions;
	
	@Column
	private String roleName;
	
	@OneToMany
	@JsonManagedReference(value="surv-resps")
	private List<Response> responses;
	
	@Column
	private boolean isCompleted;
	
	@Override
	public boolean equals(Object other) {
		Survey survey = (Survey)other;
		return this.employee.equals(survey.getEmployee()) && this.roleName.equals(survey.getRoleName());
	}
	
}
