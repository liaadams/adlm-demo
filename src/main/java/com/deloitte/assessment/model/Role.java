package com.deloitte.assessment.model;

import java.util.Collection;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;

@Entity
@Table
@Data
//@EqualsAndHashCode(exclude="employees")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Role {

	@Id
	@GeneratedValue
	@Column
	private Long id;
	
	@Column
	private String name;
	
	@ManyToMany
//	@JsonManagedReference(value="role-rt")
	private Set<RoleType> roleTypes;
	
//	@Column
//	private boolean program;
//	
//	@Column
//	private boolean delivery;
	
//	@Column
//	private boolean agileProg;
//	
//	@Column
//	private boolean agileDeliv;
//	
//	@Column
//	private boolean devopProg;
//	
//	@Column
//	private boolean devopDeliv;
	
//	@OneToMany(cascade = {CascadeType.ALL}, mappedBy="role", fetch=FetchType.EAGER)
//	@JsonManagedReference(value="role-emps")
	@ManyToMany
//	@JsonBackReference(value="role-emps")
	private Collection<Employee> employees;
	
//	@OneToMany
//	@JsonManagedReference(value="emp-survs")
//	private List<Survey> surveys;
	
//	@ManyToMany
////	@OneToMany(cascade = {CascadeType.ALL}, mappedBy="role", fetch=FetchType.EAGER)
////	@JsonManagedReference(value="role-caps")
//	private Set<Capability> capabilities;

}
