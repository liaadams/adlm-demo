package com.deloitte.assessment.web;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.deloitte.assessment.model.Question;
import com.deloitte.assessment.model.Response;
import com.deloitte.assessment.model.Survey;
import com.deloitte.assessment.model.TeamType;
import com.deloitte.assessment.service.EmployeeService;
import com.deloitte.assessment.service.OrganizationService;
import com.deloitte.assessment.service.QuestionService;
import com.deloitte.assessment.service.ResponseService;
import com.deloitte.assessment.service.SurveyService;

@Controller
@RequestMapping("/survey")
public class SurveyController {

	@Autowired
	private QuestionService questionService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private OrganizationService orgService;
	
	@Autowired
	private SurveyService surveyService;
	
	@Autowired
	private ResponseService responseService;
	
	@GetMapping
	public String test() {
		return "react-survey";
	}

	@GetMapping("/{uuid}")
	public String getSurvey(@PathVariable String uuid, Map<String, Object> model) {
		Survey survey = surveyService.getSurvey(uuid);

		if (survey == null) {
			model.put("message", "Invalid url, please contact your supervisor");
			return "error";
		}
		if(survey.isCompleted()) {
			model.put("message", "Survey already completed for employee");
			return "error";
		}

//		String roleName = emp.getRoles().get(0).getName();
		model.put("role", survey.getRoleName());
//		model.put("surveyType", "role");
		model.put("uuid", uuid);
		return "survey";
	}

//	@GetMapping("/team/{teamName}/{empId}")
//	public String getLevelSurvey(@PathVariable String teamName, @PathVariable String empId, Map<String, Object> model) {
//		Employee emp = employeeService.getEmployee(empId);
//		if (emp == null) {
//			model.put("message", "Invalid url, please contact your supervisor");
//			return "error";
//		}
//		Organization organization = emp.getSegment().getOrganization();
//		TeamType teamType = Arrays.asList(TeamType.values()).stream().filter(t -> t.getName().equals(teamName))
//				.findFirst().get();
//		if (teamType.equals(TeamType.PortfolioManagement) && organization.isPortfolioSurvey()){
//			model.put("message", "Survey already completed for this level");
//			return "error";
//		}
//		else if(teamType.equals(TeamType.ProgramManagement) && organization.isProgramSurvey()) {
//			model.put("message", "Survey already completed for this level");
//			return "error";
//		}
//		else if(teamType.equals(TeamType.Delivery) && organization.isTeamSurvey()) {
//			model.put("message", "Survey already completed for this level");
//			return "error";
//		}
//		else if(teamType.equals(TeamType.ContinuousDelivery) && organization.isContDelivSurvey()) {
//			model.put("message", "Survey already completed for this level");
//			return "error";
//		}
//			
//		model.put("team", teamName);
//		model.put("uuid", empId);
//		model.put("surveyType", "team");
//		return "team-survey";
//	}

	@GetMapping("/role/{roleName}")
	public String getRoleSurvey(@PathVariable String roleName, Map<String, Object> model) {
		model.put("role", roleName);
		return "survey";
	}

	@GetMapping("/team/{teamName}")
	public String getTeamSurvey(@PathVariable String teamName, Map<String, Object> model) {
		model.put("team", teamName);
		return "survey";
	}

	@RequestMapping(value = "/{uuid}", method = RequestMethod.POST)
	public String postAnswers(@PathVariable String uuid, @RequestBody List<Response> responses) {
		Survey survey = surveyService.getSurvey(uuid);
		if (survey == null) {
			// model.put("message", "Invalid url, please contact IT");
			return "error";
		}
//		Organization organization = employee.getSegment().getOrganization();
		// responses.forEach(r -> r.setEmployee(employee));
		// Set<Response> oldResponses = employee.getResponses();
		// oldResponses.addAll(responses);
		// employeeService.updateEmployee(employee);
		// if("complete".equals(action))
		// return "survey-complete";
		// else
		// return "";
//		responses.forEach(r -> r.setEmployee(employee));
//		Set<Response> oldResponses = employee.getResponses();
//		List<Response> common = oldResponses.stream().filter(responses::contains).collect(Collectors.toList());
//		oldResponses.removeAll(common);
//		oldResponses.addAll(responses);
		survey.setResponses(responses);
		TeamType tt = null;
		responses.stream().forEach(r -> r.setSurvey(survey));
		responses = responseService.saveResponses(responses);
		for (Response r : responses) {
			Question q = r.getQuestion();
			tt = q.getTeamType();
			if (q.getResponse().contains(r)) {
				q.getResponse().remove(r);
				q.getResponse().add(r);
			}
			questionService.updateQuestion(r.getQuestion());
		}
		survey.setCompleted(true);
		surveyService.updateSurvey(survey);
//		if("Portfolio Admin".equals(employee.getRoles().get(0).getName())) { 
//			organization.setPortfolioSurvey(true);
//			orgService.updateOrganization(organization);
//		}
//		else if("Program Admin".equals(employee.getRoles().get(0).getName())) { 
//			organization.setProgramSurvey(true);
//			orgService.updateOrganization(organization);
//		}
//		else if("Team Admin".equals(employee.getRoles().get(0).getName())) { 
//			organization.setTeamSurvey(true);
//			orgService.updateOrganization(organization);
//		}
//		else if("Continuous Delivery Admin".equals(employee.getRoles().get(0).getName())) {
//			organization.setContDelivSurvey(true);
//			orgService.updateOrganization(organization);
//		}
//		if ("Admin".equals(employee.getRole().getName())) {
//			if (tt.equals(TeamType.PortfolioManagement))
//				organization.setPortfolioSurvey(true);
//			else if (tt.equals(TeamType.ProgramManagement))
//				organization.setProgramSurvey(true);
//			else if (tt.equals(TeamType.Delivery))
//				organization.setDeliverySurvey(true);
//			orgService.updateOrganization(organization);
//		}
		return "survey-complete";
	}

	// @GetMapping("/{capabilityType}/{uuid}")
	// public String getSurveyMenu(@PathVariable String capabilityType,
	// @PathVariable String uuid,
	// Map<String, Object> model) {
	// Employee emp = employeeService.getEmployee(uuid);
	// if (emp == null) {
	// model.put("message", "Invalid url, please contact your supervisor");
	// return "error";
	// }
	// model.put("teamType", emp.getTeamType());
	// model.put("capabilityType", capabilityType);
	// model.put("uuid", uuid);
	// return "survey-menu";
	// }
	//
	// @GetMapping("/{surveyType}/{teamType}/{uuid}")
	// public String getQuestionnaire(@PathVariable String surveyType,
	// @PathVariable String teamType,
	// @PathVariable String uuid, Map<String, Object> model) {
	// Employee emp = employeeService.getEmployee(uuid);
	// if (emp == null) {
	// model.put("message", "Invalid url, please contact your supervisor");
	// return "error";
	// }
	// Map<String, Object> params = new HashMap<>();
	// if ("questionnaire".equals(surveyType))
	// params.put("TeamType", teamType);
	// else if ("maturity".equals(surveyType))
	// params.put("Capability", teamType);
	// Set<Question> questions = questionService.getQuestions(params);
	// model.put("questions", questions);
	// model.put("uuid", uuid);
	// model.put("surveyType", surveyType);
	// return "survey";
	// }
}
