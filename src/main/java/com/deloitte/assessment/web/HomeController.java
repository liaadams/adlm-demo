package com.deloitte.assessment.web;

import java.util.Collections;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.deloitte.assessment.model.Organization;
import com.deloitte.assessment.service.FormService;

@Controller
@RequestMapping("/")
public class HomeController {

	@Autowired
	private FormService formService;
	
	@GetMapping
    public String index() {
        return "index";
    }
	
	@GetMapping("home")
	public String home() {
		return "home";
	}
	
	@PostMapping
	public ResponseEntity<Object> submitForm(@RequestBody Organization org, Map<String, Object> model) {
		try {
			formService.insertData(org, model);
			if(model.containsKey("Error")) {
				return new ResponseEntity<>(
			              Collections.singletonList(model.get("Error")), 
			              HttpStatus.CONFLICT);
			}
			else if(model.containsKey("surveyError")) {
				return new ResponseEntity<>(
			              Collections.singletonList(model.get("surveyError")), 
			              HttpStatus.CREATED);
			}
		} catch(MessagingException ex) {
			model.put("Error", "Error in sending emails");
		}
		return new ResponseEntity<>(HttpStatus.CREATED);
//		return "index";
	}
	
}
