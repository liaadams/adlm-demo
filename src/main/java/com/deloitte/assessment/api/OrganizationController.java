package com.deloitte.assessment.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deloitte.assessment.model.Organization;
import com.deloitte.assessment.service.OrganizationService;

@RestController
@RequestMapping("api/organizations")
public class OrganizationController {

	@Autowired
	private OrganizationService orgService;
	
	@GetMapping
	public List<Organization> getOrganizations() {
		return orgService.getOrganizations();
	}
}
