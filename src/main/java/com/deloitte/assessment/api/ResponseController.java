package com.deloitte.assessment.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.deloitte.assessment.model.Organization;
import com.deloitte.assessment.model.Response;
import com.deloitte.assessment.model.Survey;
import com.deloitte.assessment.service.EmployeeService;
import com.deloitte.assessment.service.OrganizationService;
import com.deloitte.assessment.service.QuestionService;
import com.deloitte.assessment.service.ResponseService;
import com.deloitte.assessment.service.SegmentService;
import com.deloitte.assessment.service.SurveyService;

@RestController
@RequestMapping("api/responses")
public class ResponseController {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private QuestionService questionService;

	@Autowired
	private ResponseService rService;

	@Autowired
	private SegmentService segService;
	
	@Autowired
	private SurveyService surveyService;
	
	@Autowired
	private OrganizationService orgService;

	private static final Logger logger = LoggerFactory.getLogger(ResponseController.class);

	@RequestMapping(value = "/{uuid}", method = RequestMethod.POST)
	public void addResponses(@PathVariable String uuid, @RequestBody List<Response> responses) {
		Survey survey = surveyService.getSurvey(uuid);
		if (survey == null) {
			// model.put("message", "Invalid url, please contact IT");
			return;
		}
		// responses.forEach(r -> r.setEmployee(employee));
//		Set<Response> oldResponses = employee.getResponses();
//		List<Response> common = oldResponses.stream().filter(responses::contains).collect(Collectors.toList());
//		oldResponses.removeAll(common);
//		oldResponses.addAll(responses);
		survey.setResponses(responses);
		surveyService.updateSurvey(survey);
//		for (Response r : responses) {
//			System.out.println("content " + r.getContent());
//			Question q = r.getQuestion();
//			r.setEmployee(employee);
//			if (q.getResponse().contains(r)) {
//				q.getResponse().remove(r);
//				rService.delete(r);
//				q.getResponse().add(r);
//			}
//			questionService.updateQuestion(r.getQuestion());
//		}
//
//		employeeService.updateEmployee(employee);
	}

	@GetMapping(value="/export/{organization}")
	public void exportResponses(@PathVariable String organization, HttpServletResponse response) throws IOException {
			Organization org = orgService.getOrganization(organization);
			Workbook wb = rService.getByOrganization(org);
			response.setContentType("application/vnd.openxml");
			response.setHeader("Content-Disposition", "attachment; filename=test.xlsx");
			wb.write( response.getOutputStream() );
			wb.close();
	}
}
