package com.deloitte.assessment.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deloitte.assessment.model.Role;
import com.deloitte.assessment.service.RoleService;

@RestController
@RequestMapping("api/roles")
public class RoleController {

	@Autowired
	private RoleService roleService;
	
	@GetMapping
	public List<Role> getRoles() {
		return roleService.getRoles();
	}
}
