package com.deloitte.assessment.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deloitte.assessment.model.Employee;
import com.deloitte.assessment.model.Survey;
import com.deloitte.assessment.service.EmployeeService;
import com.deloitte.assessment.service.SurveyService;

@RestController
@RequestMapping("api/surveys")
public class SurveyRestController {

	@Autowired
	private SurveyService surveyService;
	
	@Autowired
	private EmployeeService empService;
	
	@GetMapping("/emp/{uuid}")
	public List<Survey> getSurveys(@PathVariable String uuid) {
		Employee emp = empService.getEmployee(uuid);
		return surveyService.getSurveys(emp);
	}
	
	@GetMapping("/{uuid}")
	public Survey getSurvey(@PathVariable String uuid) {
		return surveyService.getSurvey(uuid);
	}
}
