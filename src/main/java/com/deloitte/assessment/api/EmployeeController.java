package com.deloitte.assessment.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deloitte.assessment.model.Employee;
import com.deloitte.assessment.service.EmployeeService;

@RestController
@RequestMapping("api/employees")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	@GetMapping("/{uuid}")
	public Employee getEmployee(@PathVariable String uuid) {
		Employee emp = employeeService.getEmployee(uuid);
		return emp;
	}
}
