package com.deloitte.assessment.api;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.deloitte.assessment.model.TeamType;

@RestController
@RequestMapping("/api/teams")
public class TeamTypeController {

	@RequestMapping(method=RequestMethod.GET)
	public Map<TeamType, String> getTeamNames() {
		Map<TeamType, String> enums = new HashMap<>();
		Arrays.asList(TeamType.values()).forEach(t -> enums.put(t, t.getName()));
		return enums;
	}
	
}
	