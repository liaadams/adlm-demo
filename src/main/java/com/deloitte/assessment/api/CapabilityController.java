package com.deloitte.assessment.api;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.deloitte.assessment.model.Capability;
import com.deloitte.assessment.service.CapabilityService;

@RestController
@RequestMapping("api/capabilities")
public class CapabilityController {

	@Autowired
	private CapabilityService capService;
	
	@GetMapping
	public List<Capability> getCapabilities(@RequestParam Map<String, Object> params) {
		List<Capability> caps = capService.getAll();
		return caps;
	}
}
