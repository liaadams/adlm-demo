package com.deloitte.assessment.api;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.deloitte.assessment.model.Question;
import com.deloitte.assessment.service.QuestionService;

@RestController
@RequestMapping("api/questions")
public class QuestionController {

	@Autowired
	private QuestionService questionService;
	
	@GetMapping
	public List<Question> getQuestions(@RequestParam Map<String, Object> params) {
		List<Question> questions = questionService.getQuestions(params);
		return questions;
	}
	
	@GetMapping("/page")
	public Page<Question> getQuestionPage(@RequestParam Map<String, Object> params) {
		Page<Question> questions = questionService.getQuestionPage(params);
		return questions;
	}
}
