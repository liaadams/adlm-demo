module.exports = function(grunt) {

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt
			.initConfig({
				'pkg': grunt.file.readJSON('package.json'),
				
				'jshint' : {
					'beforeconcat' : [ 'src/main/resources/static/app/**/*.js' ],
				},

				'concat' : {
					'dist' : {
						'src' : [ 'src/main/resources/**/*.js' ],
						'dest' : 'src/main/resources/static/min/<%= pkg.namelower %>-<%= pkg.version %>.js'
					}
				},

				'uglify' : {
					'options' : {
						'mangle' : false
					},
					'dist' : {
						'files' : {
							'src/main/resources/static/min/<%= pkg.namelower %>-<%= pkg.version %>.min.js' : [ 'src/main/resources/static/min/<%= pkg.namelower %>-<%= pkg.version %>.js' ]
						}
					}
				},
			});

//	grunt.registerTask('default', [ 'jshint', 'concat', 'uglify' ]);
	grunt.registerTask('default', [ 'concat', 'uglify' ]);
}